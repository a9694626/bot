import logging
import os
from telegram import Update, Bot
from telegram.ext import Application, CommandHandler, CallbackContext, MessageHandler, filters

# Логирование
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

# Токен бота
TOKEN = '6369236429:AAGbll1spoiDI_pdV0vWeAf6bP_Y0nZLM3Y'

# Идентификатор администратора
ADMIN_ID = 1071569755

# Файл для хранения chat ID пользователей
CHAT_IDS_FILE = 'IDS.txt'


# Функция для чтения chat ID из файла
def load_chat_ids() -> list:
    if os.path.exists(CHAT_IDS_FILE):
        with open(CHAT_IDS_FILE, 'r') as file:
            return [int(line.strip()) for line in file]
    return []


# Функция для записи chat ID в файл
def save_chat_id_to_file(chat_id: int):
    chat_ids = load_chat_ids()
    if chat_id not in chat_ids:
        chat_ids.append(chat_id)
        with open(CHAT_IDS_FILE, 'w') as file:  # Перезаписываем файл каждый раз полностью
            for id in chat_ids:
                file.write(f"{id}\n")


# Функция для команды /start
async def start(update: Update, context: CallbackContext) -> None:
    chat_id = update.message.chat_id
    save_chat_id_to_file(chat_id)
    await update.message.reply_text("Приветик, любитель капибар!! У меня для тебя есть подарочек --> /get_file.")


# Функция для команды /get_file
async def get_file(update: Update, context: CallbackContext) -> None:
    chat_id = update.message.chat_id
    save_chat_id_to_file(chat_id)
    logging.info(f"Команда /get_file от пользователя {chat_id}")
    # Отправка файла
    try:
        with open('капибарыш.png', 'rb') as file:
            await context.bot.send_document(chat_id=chat_id, document=file, filename='капибарыш.png')
            await update.message.reply_text(
                "Это капибара Борис. Он любит вкусно покушать и крепко поспать (все мы его понимаем..). Проходи тест на то, какая ты мемная капибара вместе с ним по ссылке -->> ")
            await update.message.reply_text("https://uquiz.com/quiz/edYOGN/%D0%BA%D0%B0%D0%BA%D0%B0%D1%8F-%D0%B2%D1%8B-%D0%BC%D0%B5%D0%BC%D0%BD%D0%B0%D1%8F-%D0%BA%D0%B0%D0%BF%D0%B8%D0%B1%D0%B0%D1%80%D0%B0")
            await update.message.reply_text(
                "Ты понравился капибаре Борису!! Капибара Борис дарит тебе скидку 10% на все капибара-игрушки в нашем уютном телеграм канале -->> @kapibarus_rf")
            await update.message.reply_text(
                "Скорее подписывайся <3")
        logging.info("Файл успешно отправлен.")
    except Exception as e:
        logging.error(f"Ошибка при отправке файла: {e}")


# Функция для отправки рассылок
async def broadcast_message(message: str, bot: Bot, chat_ids: list) -> None:
    for chat_id in chat_ids:
        try:
            await bot.send_message(chat_id=chat_id, text=message)
            logging.info(f"Сообщение отправлено пользователю {chat_id}")
        except Exception as e:
            logging.error(f"Ошибка при отправке сообщения пользователю {chat_id}: {e}")


# Функция only для админа
async def admin_broadcast(update: Update, context: CallbackContext) -> None:
    if update.message.chat_id != ADMIN_ID:
        await update.message.reply_text("У вас нет прав для выполнения этой команды.")
        return

    if len(context.args) == 0:
        await update.message.reply_text("Пожалуйста, укажите сообщение для рассылки.")
        return

    # Сообщение для рассылки
    message = " ".join(context.args)

    # Получение всех пользователей бота
    chat_ids = load_chat_ids()

    await broadcast_message(message, context.bot, chat_ids)
    await update.message.reply_text("Сообщение отправлено всем пользователям.")


# Главная функция
def main():
    application = Application.builder().token(TOKEN).build()

    # Регистрация обработчиков команд
    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("get_file", get_file))
    application.add_handler(CommandHandler("broadcast", admin_broadcast))

    # Обработчик текстовых сообщений для сохранения chat_id
    application.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, save_chat_id_to_file))

    # Запуск бота
    application.run_polling()


if __name__ == '__main__':
    main()
